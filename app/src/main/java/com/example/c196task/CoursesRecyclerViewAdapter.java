package com.example.c196task;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.util.ArrayList;

public class CoursesRecyclerViewAdapter extends RecyclerView.Adapter {

    private static final String TAG = "CoursesRecyclerViewAdapter";

    private ArrayList<Course> courses;
    private Context context;
    private DatabaseHelper db;

    public CoursesRecyclerViewAdapter(ArrayList<Course> courses, Context context) {
        this.courses = courses;
        this.context = context;
        db = DatabaseHelper.getInstance(this.context);
    }

    @NonNull
    @Override
    public CoursesRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_course, parent, false);
        CoursesRecyclerViewAdapter.ViewHolder holder = new CoursesRecyclerViewAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull @org.jetbrains.annotations.NotNull RecyclerView.ViewHolder holder, int position) {
        CoursesRecyclerViewAdapter.ViewHolder viewHolder = (CoursesRecyclerViewAdapter.ViewHolder) holder;
        viewHolder.recyclerCourseTitle.setText(courses.get(position).getTitle());
        if (!courses.get(position).getCourseInstructors().isEmpty()) {
            viewHolder.recyclerCourseInstructor.setText(courses.get(position).getCourseInstructors().get(0).getName());
        } else {
            viewHolder.recyclerCourseInstructor.setText("No instructor");
        }
        if (courses.get(position).getStartDate() == null) {
            viewHolder.recyclerCourseDate.setText("No start date entered");
        } else {
            DateFormat df = DateFormat.getDateInstance();
            viewHolder.recyclerCourseDate.setText(df.format(courses.get(position).getStartDate()) + " - " + df.format(courses.get(position).getEndDate()));
        }
        Course.Status status = courses.get(position).getStatus();
        switch (status) {
            case COMPLETED:
                viewHolder.recyclerCourseStatus.setText("Completed");
                break;
            case DROPPED:
                viewHolder.recyclerCourseStatus.setText("Dropped");
                break;
            case IN_PROGRESS:
                viewHolder.recyclerCourseStatus.setText("In Progress");
                break;
            case PLAN_TO_TAKE:
                viewHolder.recyclerCourseStatus.setText("Plan to take");
                break;
            case UNKNOWN:
                viewHolder.recyclerCourseStatus.setText("Unknown");
                break;
        }
        viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CourseDetailedView.class);
                intent.putExtra("COURSE_ID", courses.get(position).getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return courses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView recyclerCourseTitle;
        TextView recyclerCourseInstructor;
        TextView recyclerCourseDate;
        TextView recyclerCourseStatus;
        RelativeLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            recyclerCourseTitle = itemView.findViewById(R.id.course_recycler_entry_name);
            recyclerCourseInstructor = itemView.findViewById(R.id.course_recycler_instructor_name);
            recyclerCourseDate = itemView.findViewById(R.id.course_recycler_start_end_date);
            recyclerCourseStatus = itemView.findViewById(R.id.course_recycler_status);
            parentLayout = itemView.findViewById(R.id.course_list_item_layout);
        }
    }
}