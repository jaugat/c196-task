package com.example.c196task;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class CreateCourseActivity extends AppCompatActivity {

    private EditText titleField;
    private TextView nameFieldError;
    private Spinner startDateMonthSpinner;
    private Spinner startDateDaySpinner;
    private Spinner startDateYearSpinner;
    private TextView dateError;
    private Spinner endDateMonthSpinner;
    private Spinner endDateDaySpinner;
    private Spinner endDateYearSpinner;
    private Spinner statusSpinner;
    private RecyclerView addAssessmentRecyclerView;
    private RecyclerView addInstructorRecyclerView;
    private Button createButton;
    private DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_course);
        db = DatabaseHelper.getInstance(this);
        Toolbar toolbar = findViewById(R.id.create_course_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        titleField = findViewById(R.id.create_course_name_field);
        nameFieldError = findViewById(R.id.create_course_name_error);
        startDateMonthSpinner = findViewById(R.id.create_course_start_date_month_spinner);
        ArrayAdapter<CharSequence> startDateMonthAdapter = ArrayAdapter.createFromResource(this,
                R.array.date_month_list, android.R.layout.simple_spinner_item);
        startDateMonthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        startDateMonthSpinner.setAdapter(startDateMonthAdapter);
        startDateDaySpinner = findViewById(R.id.create_course_start_date_day_spinner);
        ArrayAdapter<CharSequence> startDateDayAdapter = ArrayAdapter.createFromResource(this,
                R.array.date_day_list, android.R.layout.simple_spinner_item);
        startDateDayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        startDateDaySpinner.setAdapter(startDateDayAdapter);

        startDateYearSpinner = findViewById(R.id.create_course_start_date_year_spinner);
        ArrayAdapter<CharSequence> startDateYearAdapter = ArrayAdapter.createFromResource(this,
                R.array.date_year_list, android.R.layout.simple_spinner_item);
        startDateYearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        startDateYearSpinner.setAdapter(startDateYearAdapter);

        dateError = findViewById(R.id.create_course_date_error);
        endDateMonthSpinner = findViewById(R.id.create_course_end_date_month_spinner);
        ArrayAdapter<CharSequence> endDateMonthAdapter = ArrayAdapter.createFromResource(this,
                R.array.date_month_list, android.R.layout.simple_spinner_item);
        endDateMonthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        endDateMonthSpinner.setAdapter(endDateMonthAdapter);

        endDateDaySpinner = findViewById(R.id.create_course_end_date_day_spinner);
        ArrayAdapter<CharSequence> endDateDayAdapter = ArrayAdapter.createFromResource(this,
                R.array.date_day_list, android.R.layout.simple_spinner_item);
        endDateDayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        endDateDaySpinner.setAdapter(endDateDayAdapter);
        endDateYearSpinner = findViewById(R.id.create_course_end_date_year_spinner);
        ArrayAdapter<CharSequence> endDateYearAdapter = ArrayAdapter.createFromResource(this,
                R.array.date_year_list, android.R.layout.simple_spinner_item);
        endDateYearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        endDateYearSpinner.setAdapter(endDateYearAdapter);
        statusSpinner = findViewById(R.id.create_course_status_spinner);
        ArrayAdapter<CharSequence> statusSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.course_status_list, android.R.layout.simple_spinner_item);
        statusSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        statusSpinner.setAdapter(statusSpinnerAdapter);
        addAssessmentRecyclerView = findViewById(R.id.create_course_add_assessment_recycler);
        AddAssessmentRecyclerAdapter addAssessmentsAdapter = new AddAssessmentRecyclerAdapter(db.getSavedAssessments(), this);
        addAssessmentRecyclerView.setAdapter(addAssessmentsAdapter);
        addAssessmentRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        addInstructorRecyclerView = findViewById(R.id.create_course_add_instructor_recycler);
        AddInstructorRecyclerAdapter addInstructorAdapter = new AddInstructorRecyclerAdapter(db.getSavedInstructors(), this);
        addInstructorRecyclerView.setAdapter(addInstructorAdapter);
        addInstructorRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        createButton = findViewById(R.id.create_course_button);
        createButton.setOnClickListener(v -> {
            Course course = new Course();
            dateError.setText("");
            nameFieldError.setText("");
            switch (statusSpinner.getSelectedItem().toString()) {
                case "In Progress":
                    course.setStatus("IN_PROGRESS");
                    break;
                case "Completed":
                    course.setStatus("COMPLETED");
                    break;
                case "Plan to Take":
                    course.setStatus("PLAN_TO_TAKE");
                    break;
                case "Dropped":
                    course.setStatus("DROPPED");
                    break;
                case "Unknown":
                    course.setStatus("UNKNOWN");
                    break;
            }
            if (titleField.getText().toString().equals("")) {
                nameFieldError.setText("Name field can't be empty");
            } else {
                course.setTitle(titleField.getText().toString());
                try {
                    LocalDate startLd = LocalDate.of(Integer.parseInt(startDateYearSpinner.getSelectedItem().toString()), startDateMonthSpinner.getSelectedItemPosition() + 1, Integer.parseInt(startDateDaySpinner.getSelectedItem().toString()));
                    Date startDate = Date.from(startLd.atStartOfDay(ZoneId.systemDefault()).toInstant());
                    LocalDate endLd = LocalDate.of(Integer.parseInt(endDateYearSpinner.getSelectedItem().toString()), endDateMonthSpinner.getSelectedItemPosition() + 1, Integer.parseInt(endDateDaySpinner.getSelectedItem().toString()));
                    Date endDate = Date.from(endLd.atStartOfDay(ZoneId.systemDefault()).toInstant());
                    if (startDate.after(endDate)) {
                        dateError.setText("Start date must be before end date");
                    } else {
                        course.setStartDate(startDate);
                        course.setEndDate(endDate);
                        course.setAssessments(addAssessmentsAdapter.getCheckedAssessments());
                        course.setCourseInstructors(addInstructorAdapter.getCheckedInstructors());
                        int id = (int) db.addCourse(course);
                        course.setId(id);
                        db.updateAssignedAssessments(course);
                        db.updateAssignedInstructors(course);
                        Toast.makeText(this, "Course successfully created", Toast.LENGTH_LONG).show();

                        Intent intent = new Intent(this, MainActivity.class);
                        intent.putExtra("FRAGMENT_LAYOUT_ID", R.layout.fragment_courses);
                        startActivity(intent);
                    }
                } catch (DateTimeException e) {
                    dateError.setText("Invalid date(s) entered");
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}