package com.example.c196task;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.ContactsContract;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.text.DateFormat;
import java.util.Date;

public class AlertReceiver extends BroadcastReceiver {

    private NotificationManagerCompat notificationManagerCompat;
    private DatabaseHelper db;
    private DateFormat df;

    @Override
    public void onReceive(Context context, Intent intent) {
        db = DatabaseHelper.getInstance(context);
        df = DateFormat.getDateInstance();
        String dateType = intent.getStringExtra("DATE_TYPE");
        switch (intent.getStringExtra("OBJECT_TYPE")) {
            case "COURSE":
                Course c = db.getCourse(intent.getIntExtra("COURSE_ID", -1));
                notificationManagerCompat = NotificationManagerCompat.from(context);
                Notification notification1;
                if (dateType == "Start date") {
                    notification1 = new NotificationCompat.Builder(context, NotificationHelper.ALARM_CHANNEL)
                            .setSmallIcon(R.drawable.ic_launcher_foreground)
                            .setContentTitle(c.getTitle())
                            .setContentText(intent.getStringExtra("DATE_TYPE") + " is on " + df.format(c.getStartDate()))
                            .setCategory(NotificationCompat.CATEGORY_ALARM)
                            .build();
                } else {
                    notification1 = new NotificationCompat.Builder(context, NotificationHelper.ALARM_CHANNEL)
                            .setSmallIcon(R.drawable.ic_launcher_foreground)
                            .setContentTitle(c.getTitle())
                            .setContentText(intent.getStringExtra("DATE_TYPE") + " is on " + df.format(c.getEndDate()))
                            .setCategory(NotificationCompat.CATEGORY_ALARM)
                            .build();
                }
                notificationManagerCompat.notify(1, notification1);
                break;
            case "ASSESSMENT":
                Assessment a = db.getAssessment(intent.getIntExtra("ASSESSMENT_ID", -1));
                notificationManagerCompat = NotificationManagerCompat.from(context);
                if (dateType == "Start date") {
                    Notification notification2 = new NotificationCompat.Builder(context, NotificationHelper.ALARM_CHANNEL)
                            .setSmallIcon(R.drawable.ic_launcher_foreground)
                            .setContentTitle(a.getTitle())
                            .setContentText(intent.getStringExtra("DATE_TYPE") + " is on " + df.format(a.getStartDate()))
                            .setCategory(NotificationCompat.CATEGORY_ALARM)
                            .build();
                    notificationManagerCompat.notify(1, notification2);
                } else {
                    Notification notification2 = new NotificationCompat.Builder(context, NotificationHelper.ALARM_CHANNEL)
                            .setSmallIcon(R.drawable.ic_launcher_foreground)
                            .setContentTitle(a.getTitle())
                            .setContentText(intent.getStringExtra("DATE_TYPE") + " is on " + df.format(a.getEndDate()))
                            .setCategory(NotificationCompat.CATEGORY_ALARM)
                            .build();
                }
                break;
        }
    }
}
