package com.example.c196task;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHelper";

    private static DatabaseHelper db;

    private static final int DATABASE_VERSION = 5;

    public static final String DATABASE_NAME = "database";

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private static final String TABLE_TERMS = "terms";
    private static final String TABLE_COURSES = "courses";
    private static final String TABLE_ASSESSMENTS = "assessments";
    private static final String TABLE_INSTRUCTORS = "instructors";

    private static final String KEY_ID = "id";

    public static final String KEY_TERM_NAME = "term_name";
    public static final String KEY_TERM_START_DATE = "term_start_date";
    public static final String KEY_TERM_END_DATE = "term_end_date";

    public static final String KEY_COURSE_TITLE = "course_title";
    public static final String KEY_COURSE_START_DATE = "course_start_date";
    public static final String KEY_COURSE_END_DATE = "course_end_date";
    public static final String KEY_COURSE_STATUS = "course_status";
    public static final String KEY_COURSE_NOTE_1 = "course_note_1";
    public static final String KEY_COURSE_NOTE_2 = "course_note_2";
    public static final String KEY_COURSE_NOTE_3 = "course_note_3";
    public static final String KEY_ASSIGNED_TERM_ID = "assigned_term_id";

    public static final String KEY_ASSESSMENT_TITLE = "assessment_title";
    public static final String KEY_ASSESSMENT_TYPE = "assessment_type";
    public static final String KEY_ASSESSMENT_START_DATE = "assessment_start_date";
    public static final String KEY_ASSESSMENT_END_DATE = "assessment_end_date";
    public static final String KEY_ASSIGNED_COURSE_ID = "assigned_course_id";

    public static final String KEY_INSTRUCTOR_NAME = "instructor_name";
    public static final String KEY_INSTRUCTOR_PHONE = "instructor_phone";
    public static final String KEY_INSTRUCTOR_EMAIL = "instructor_email";

    public static final String CREATE_TABLE_TERMS = "CREATE TABLE " + TABLE_TERMS + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_TERM_NAME + " TEXT,"
            + KEY_TERM_START_DATE + " TEXT," + KEY_TERM_END_DATE + " TEXT" + ")";

    public static final String CREATE_TABLE_COURSES = "CREATE TABLE " + TABLE_COURSES + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_COURSE_TITLE + " TEXT,"
            + KEY_COURSE_START_DATE + " TEXT," + KEY_COURSE_END_DATE + " TEXT," + KEY_COURSE_STATUS + " TEXT," + KEY_COURSE_NOTE_1 + " TEXT," + KEY_COURSE_NOTE_2 + " TEXT," + KEY_COURSE_NOTE_3 + " TEXT," + KEY_ASSIGNED_TERM_ID + " INTEGER" + ")";

    public static final String CREATE_TABLE_ASSESSMENTS = "CREATE TABLE " + TABLE_ASSESSMENTS + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_ASSESSMENT_TITLE + " TEXT,"
            + KEY_ASSESSMENT_TYPE + " TEXT," + KEY_ASSESSMENT_START_DATE + " TEXT," + KEY_ASSESSMENT_END_DATE + " TEXT," + KEY_ASSIGNED_COURSE_ID + " INTEGER" + ")";

    public static final String CREATE_TABLE_INSTRUCTORS = "CREATE TABLE " + TABLE_INSTRUCTORS + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_INSTRUCTOR_NAME + " TEXT,"
            + KEY_INSTRUCTOR_PHONE + " TEXT," + KEY_INSTRUCTOR_EMAIL + " TEXT," + KEY_ASSIGNED_COURSE_ID + " INTEGER" + ")";


    public static DatabaseHelper getInstance(Context context) {
        if (db == null) {
            db = new DatabaseHelper(context);
        }
        return db;
    }

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_TERMS);
        db.execSQL(CREATE_TABLE_COURSES);
        db.execSQL(CREATE_TABLE_ASSESSMENTS);
        db.execSQL(CREATE_TABLE_INSTRUCTORS);
        ContentValues values1 = new ContentValues();
        values1.put(KEY_INSTRUCTOR_NAME, "John Doe");
        values1.put(KEY_INSTRUCTOR_EMAIL, "null@null.com");
        values1.put(KEY_INSTRUCTOR_PHONE, "888-123-4567");
        values1.put(KEY_ASSIGNED_COURSE_ID, -1);
        db.insert(TABLE_INSTRUCTORS, null, values1);
        ContentValues values2 = new ContentValues();
        values2.put(KEY_INSTRUCTOR_NAME, "Lee Young");
        values2.put(KEY_INSTRUCTOR_EMAIL, "null@null.com");
        values2.put(KEY_INSTRUCTOR_PHONE, "999-999-9999");
        values2.put(KEY_ASSIGNED_COURSE_ID, -1);
        db.insert(TABLE_INSTRUCTORS, null, values2);
        ContentValues values3 = new ContentValues();
        values3.put(KEY_INSTRUCTOR_NAME, "Debra Smith");
        values3.put(KEY_INSTRUCTOR_EMAIL, "null@null.com");
        values3.put(KEY_INSTRUCTOR_PHONE, "111-111-1111");
        values3.put(KEY_ASSIGNED_COURSE_ID, -1);
        db.insert(TABLE_INSTRUCTORS, null, values3);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TERMS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COURSES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ASSESSMENTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INSTRUCTORS);

        onCreate(db);
    }

    public ArrayList<Term> getSavedTerms() {
        ArrayList<Term> savedTerms = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_TERMS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                Term t = new Term();
                t.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                t.setName(c.getString(c.getColumnIndex(KEY_TERM_NAME)));
                try {
                    t.setStartDate(DATE_FORMAT.parse(c.getString(c.getColumnIndex(KEY_TERM_START_DATE))));
                    t.setEndDate(DATE_FORMAT.parse(c.getString(c.getColumnIndex(KEY_TERM_END_DATE))));
                } catch (ParseException e) {
                    Log.d(TAG, "getSavedTerms: could not parse dates for term ID " + t.getId());
                }
                t.setAssignedCourses(getAssignedCourses(t.getId()));
                savedTerms.add(t);
            } while (c.moveToNext());
        }
        c.close();
        return savedTerms;
    }

    public Term getTerm(int id) {
        Term t = new Term();
        String selectQuery = "SELECT * FROM " + TABLE_TERMS + " WHERE " + KEY_ID + " = " + id;
        Log.d(TAG, "getTerm called");
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            t.setId(id);
            t.setName(c.getString(c.getColumnIndex(KEY_TERM_NAME)));
            try {
                t.setStartDate(DATE_FORMAT.parse(c.getString(c.getColumnIndex(KEY_TERM_START_DATE))));
                t.setEndDate(DATE_FORMAT.parse(c.getString(c.getColumnIndex(KEY_TERM_END_DATE))));
            } catch (ParseException e) {
                Log.d(TAG, "getSavedTerms: could not parse dates for term ID " + t.getId());
            }
            t.setAssignedCourses(getAssignedCourses(id));
        }
        c.close();
        return t;
    }

    public boolean updateTerm(Term t) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TERM_NAME, t.getName());
        values.put(KEY_TERM_START_DATE, DATE_FORMAT.format(t.getStartDate()));
        values.put(KEY_TERM_END_DATE, DATE_FORMAT.format(t.getEndDate()));
        int rowsUpdated = db.update(TABLE_TERMS, values, KEY_ID + " = ?", new String[]{String.valueOf(t.getId())});
        return rowsUpdated > 0;
    }

    public long addTerm(Term t) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TERM_NAME, t.getName());
        values.put(KEY_TERM_START_DATE, DATE_FORMAT.format(t.getStartDate()));
        values.put(KEY_TERM_END_DATE, DATE_FORMAT.format(t.getEndDate()));
        long termId = db.insert(TABLE_TERMS, null, values);
        return termId;
    }

    public boolean deleteTerm(Term t) {
        if (t.getAssignedCourses().size() == 0) {
            SQLiteDatabase db = this.getWritableDatabase();
            int rowsDeleted = db.delete(TABLE_TERMS, KEY_ID + " = ?", new String[]{String.valueOf(t.getId())});
            return rowsDeleted > 0;
        } else {
            return false;
        }
    }

    public ArrayList<Course> getSavedCourses() {
        ArrayList<Course> courses = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_COURSES;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                Course course = new Course();
                course.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                course.setTitle(c.getString(c.getColumnIndex(KEY_COURSE_TITLE)));
                try {
                    course.setStartDate(DATE_FORMAT.parse(c.getString(c.getColumnIndex(KEY_COURSE_START_DATE))));
                    course.setEndDate(DATE_FORMAT.parse(c.getString(c.getColumnIndex(KEY_COURSE_END_DATE))));
                } catch (ParseException e) {
                    Log.d(TAG, "getSavedCourses: could not pars date for course id " + course.getId());
                }
                course.setStatus(c.getString(c.getColumnIndex(KEY_COURSE_STATUS)));
                course.addNote(c.getString(c.getColumnIndex(KEY_COURSE_NOTE_1)));
                course.addNote(c.getString(c.getColumnIndex(KEY_COURSE_NOTE_2)));
                course.addNote(c.getString(c.getColumnIndex(KEY_COURSE_NOTE_3)));
                course.setAssessments(getAssignedAssessments(course.getId()));
                course.setCourseInstructors(getAssignedInstructors(course.getId()));
                courses.add(course);
            } while (c.moveToNext());
        }
        c.close();
        return courses;
    }

    public Course getCourse(int id) {
        Course course = new Course();
        String selectQuery = "SELECT * FROM " + TABLE_COURSES + " WHERE " + KEY_ID + " = ?";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, new String[] {String.valueOf(id)});
        if (c.moveToFirst()) {
            course.setId(c.getInt(c.getColumnIndex(KEY_ID)));
            course.setTitle(c.getString(c.getColumnIndex(KEY_COURSE_TITLE)));
            try {
                course.setStartDate(DATE_FORMAT.parse(c.getString(c.getColumnIndex(KEY_COURSE_START_DATE))));
                course.setEndDate(DATE_FORMAT.parse(c.getString(c.getColumnIndex(KEY_COURSE_END_DATE))));
            } catch (ParseException e) {
                Log.d(TAG, "getSavedCourses: could not parse date for course id " + course.getId());
            }
            course.setStatus(c.getString(c.getColumnIndex(KEY_COURSE_STATUS)));
            course.addNote(c.getString(c.getColumnIndex(KEY_COURSE_NOTE_1)));
            course.addNote(c.getString(c.getColumnIndex(KEY_COURSE_NOTE_2)));
            course.addNote(c.getString(c.getColumnIndex(KEY_COURSE_NOTE_3)));
            course.setAssessments(getAssignedAssessments(course.getId()));
            course.setCourseInstructors(getAssignedInstructors(course.getId()));
        }
        c.close();
        return course;
    }

    public Course getCourse(String title) {
        Course course = new Course();
        String selectQuery = "SELECT * FROM " + TABLE_COURSES + " WHERE " + KEY_COURSE_TITLE + " = ?";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, new String[] {title});
        if (c.moveToFirst()) {
            course.setId(c.getInt(c.getColumnIndex(KEY_ID)));
            course.setTitle(c.getString(c.getColumnIndex(KEY_COURSE_TITLE)));
            try {
                course.setStartDate(DATE_FORMAT.parse(c.getString(c.getColumnIndex(KEY_COURSE_START_DATE))));
                course.setEndDate(DATE_FORMAT.parse(c.getString(c.getColumnIndex(KEY_COURSE_END_DATE))));
            } catch (ParseException e) {
                Log.d(TAG, "getSavedCourses: could not parse date for course id " + course.getId());
            }
            course.setStatus(c.getString(c.getColumnIndex(KEY_COURSE_STATUS)));
            course.addNote(c.getString(c.getColumnIndex(KEY_COURSE_NOTE_1)));
            course.addNote(c.getString(c.getColumnIndex(KEY_COURSE_NOTE_2)));
            course.addNote(c.getString(c.getColumnIndex(KEY_COURSE_NOTE_3)));
            course.setAssessments(getAssignedAssessments(course.getId()));
            course.setCourseInstructors(getAssignedInstructors(course.getId()));
        }
        c.close();
        return course;
    }

    public ArrayList<Course> getAssignedCourses(int id) {
        ArrayList<Course> courses = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_COURSES + " WHERE " + KEY_ASSIGNED_TERM_ID + " = " + id;
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                Course course = new Course();
                course.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                course.setTitle(c.getString(c.getColumnIndex(KEY_COURSE_TITLE)));
                try {
                    course.setStartDate(DATE_FORMAT.parse(c.getString(c.getColumnIndex(KEY_COURSE_START_DATE))));
                    course.setEndDate(DATE_FORMAT.parse(c.getString(c.getColumnIndex(KEY_COURSE_END_DATE))));
                } catch (ParseException e) {
                    Log.d(TAG, "getSavedCourses: could not pars date for course id " + course.getId());
                }
                course.setStatus(c.getString(c.getColumnIndex(KEY_COURSE_STATUS)));
                course.addNote(c.getString(c.getColumnIndex(KEY_COURSE_NOTE_1)));
                course.addNote(c.getString(c.getColumnIndex(KEY_COURSE_NOTE_2)));
                course.addNote(c.getString(c.getColumnIndex(KEY_COURSE_NOTE_3)));
                course.setAssessments(getAssignedAssessments(course.getId()));
                course.setCourseInstructors(getAssignedInstructors(course.getId()));
                course.setAssignedTermId(id);
                courses.add(course);
            } while (c.moveToNext());
        }
        c.close();
        return courses;
    }

    public boolean updateCourse(Course c) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_COURSE_TITLE, c.getTitle());
        values.put(KEY_COURSE_START_DATE, DATE_FORMAT.format(c.getStartDate()));
        values.put(KEY_COURSE_END_DATE, DATE_FORMAT.format(c.getEndDate()));
        switch (c.getStatus()) {
            case COMPLETED:
                values.put(KEY_COURSE_STATUS, "COMPLETED");
                break;
            case IN_PROGRESS:
                values.put(KEY_COURSE_STATUS, "IN_PROGRESS");
                break;
            case DROPPED:
                values.put(KEY_COURSE_STATUS, "DROPPED");
                break;
            case PLAN_TO_TAKE:
                values.put(KEY_COURSE_STATUS, "PLAN_TO_TAKE");
                break;
            case UNKNOWN:
                values.put(KEY_COURSE_STATUS, "UNKNOWN");
                break;
        }
        values.put(KEY_COURSE_NOTE_1, c.getNote(0));
        values.put(KEY_COURSE_NOTE_2, c.getNote(1));
        values.put(KEY_COURSE_NOTE_3, c.getNote(2));
        values.put(KEY_ASSIGNED_TERM_ID, c.getAssignedTermId());
        int rowsUpdated = db.update(TABLE_COURSES, values, KEY_ID + " = ?", new String[]{String.valueOf(c.getId())});
        return rowsUpdated > 0;
    }

    public boolean updateAssignedCourses(Term t) {
        int rowsUpdated = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        for (Course course : getAssignedCourses(t.getId())) {
            ContentValues values = new ContentValues();
            values.put(KEY_ASSIGNED_TERM_ID, -1);
            rowsUpdated += db.update(TABLE_COURSES, values, KEY_ID + " = ?", new String[] {String.valueOf(course.getId())});
        }

        for (Course course : t.getAssignedCourses()) {
                ContentValues values = new ContentValues();
                values.put(KEY_ASSIGNED_TERM_ID, t.getId());
                rowsUpdated += db.update(TABLE_COURSES, values, KEY_ID + " = ?", new String[] {String.valueOf(course.getId())});
        }
        return rowsUpdated > 0;
    }

    public long addCourse(Course c) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_COURSE_TITLE, c.getTitle());
        values.put(KEY_COURSE_START_DATE, DATE_FORMAT.format(c.getStartDate()));
        values.put(KEY_COURSE_END_DATE, DATE_FORMAT.format(c.getEndDate()));
        for (int i = 0; i < c.getAllNotes().size(); i++) {
            switch (i) {
                case 0:
                    values.put(KEY_COURSE_NOTE_1, c.getNote(0));
                    break;
                case 1:
                    values.put(KEY_COURSE_NOTE_2, c.getNote(1));
                    break;
                case 2:
                    values.put(KEY_COURSE_NOTE_3, c.getNote(2));
                    break;
            }
        }
        switch (c.getStatus()) {
            case COMPLETED:
                values.put(KEY_COURSE_STATUS, "COMPLETED");
                break;
            case IN_PROGRESS:
                values.put(KEY_COURSE_STATUS, "IN_PROGRESS");
                break;
            case DROPPED:
                values.put(KEY_COURSE_STATUS, "DROPPED");
                break;
            case PLAN_TO_TAKE:
                values.put(KEY_COURSE_STATUS, "PLAN_TO_TAKE");
                break;
            case UNKNOWN:
                values.put(KEY_COURSE_STATUS, "UNKNOWN");
                break;
        }
        values.put(KEY_ASSIGNED_TERM_ID, -1);
        long courseId = db.insert(TABLE_COURSES, null, values);
        return courseId;
    }

    public boolean deleteCourse(Course c) {
        if (c.getAssessments().size() == 0) {
            SQLiteDatabase db = this.getWritableDatabase();
            int rowsDeleted = db.delete(TABLE_COURSES, KEY_ID + " = ?", new String[]{String.valueOf(c.getId())});
            return rowsDeleted > 0;
        } else {
            return false;
        }
    }

    public ArrayList<Assessment> getSavedAssessments() {
        ArrayList<Assessment> assessments = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_ASSESSMENTS;
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                Assessment a = new Assessment();
                a.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                a.setTitle(c.getString(c.getColumnIndex(KEY_ASSESSMENT_TITLE)));
                try {
                    a.setStartDate(DATE_FORMAT.parse(c.getString(c.getColumnIndex(KEY_ASSESSMENT_START_DATE))));
                    a.setEndDate(DATE_FORMAT.parse(c.getString(c.getColumnIndex(KEY_ASSESSMENT_END_DATE))));
                } catch (ParseException e) {
                    Log.d(TAG, "getSavedAssessments: cannot parse date for assessment ID " + a.getId());
                }
                a.setType(c.getString(c.getColumnIndex(KEY_ASSESSMENT_TYPE)));
                a.setAssignedCourseId(c.getInt(c.getColumnIndex(KEY_ASSIGNED_COURSE_ID)));
                assessments.add(a);
            } while (c.moveToNext());
        }
        c.close();
        return assessments;
    }

    public Assessment getAssessment(int id) {
        Assessment a = new Assessment();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_ASSESSMENTS + " WHERE " + KEY_ID + " = ?";
        Cursor c = db.rawQuery(selectQuery, new String[] {String.valueOf(id)});
        if (c.moveToFirst()) {
            a.setId(c.getInt(c.getColumnIndex(KEY_ID)));
            a.setTitle(c.getString(c.getColumnIndex(KEY_ASSESSMENT_TITLE)));
            try {
                a.setStartDate(DATE_FORMAT.parse(c.getString(c.getColumnIndex(KEY_ASSESSMENT_START_DATE))));
                a.setEndDate(DATE_FORMAT.parse(c.getString(c.getColumnIndex(KEY_ASSESSMENT_END_DATE))));
            } catch (ParseException e) {
                Log.d(TAG, "getSavedAssessments: cannot parse date for assessment ID " + a.getId());
            }
            a.setType(c.getString(c.getColumnIndex(KEY_ASSESSMENT_TYPE)));
            a.setAssignedCourseId(c.getInt(c.getColumnIndex(KEY_ASSIGNED_COURSE_ID)));
        }
        c.close();
        return a;
    }

    public ArrayList<Assessment> getAssignedAssessments(int id) {
        ArrayList<Assessment> assessments = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_ASSESSMENTS + " WHERE " + KEY_ASSIGNED_COURSE_ID + " = ?";
        Cursor c = db.rawQuery(selectQuery, new String[]{String.valueOf(id)});
        if (c.moveToFirst()) {
            do {
                Assessment a = new Assessment();
                a.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                a.setTitle(c.getString(c.getColumnIndex(KEY_ASSESSMENT_TITLE)));
                try {
                    a.setStartDate(DATE_FORMAT.parse(c.getString(c.getColumnIndex(KEY_ASSESSMENT_START_DATE))));
                    a.setEndDate(DATE_FORMAT.parse(c.getString(c.getColumnIndex(KEY_ASSESSMENT_END_DATE))));
                } catch (ParseException e) {
                    Log.d(TAG, "getSavedAssessments: cannot parse date for assessment ID " + a.getId());
                }
                a.setType(c.getString(c.getColumnIndex(KEY_ASSESSMENT_TYPE)));
                a.setAssignedCourseId(id);
                assessments.add(a);
            } while (c.moveToNext());
        }
        c.close();
        return assessments;
    }

    public boolean updateAssessment(Assessment a) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ASSESSMENT_TITLE, a.getTitle());
        values.put(KEY_ASSESSMENT_START_DATE, DATE_FORMAT.format(a.getStartDate()));
        values.put(KEY_ASSESSMENT_END_DATE, DATE_FORMAT.format(a.getEndDate()));
        switch (a.getType()) {
            case PERFORMANCE:
                values.put(KEY_ASSESSMENT_TYPE, "PERFORMANCE");
                break;
            case OBJECTIVE:
                values.put(KEY_ASSESSMENT_TYPE, "OBJECTIVE");
                break;
        }
        int rowsUpdated = db.update(TABLE_ASSESSMENTS, values, KEY_ID + " = ?", new String[]{String.valueOf(a.getId())});
        return rowsUpdated > 0;
    }

    public boolean updateAssignedAssessments(Course course) {
        int rowsUpdated = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        for (Assessment a : getAssignedAssessments(course.getId())) {
            ContentValues values = new ContentValues();
            values.put(KEY_ASSIGNED_COURSE_ID, -1);
            rowsUpdated += db.update(TABLE_ASSESSMENTS, values, KEY_ID + " = ?", new String[] {String.valueOf(a.getId())});
        }

        for (Assessment a : course.getAssessments()) {
                ContentValues values = new ContentValues();
                values.put(KEY_ASSIGNED_COURSE_ID, course.getId());
                rowsUpdated += db.update(TABLE_ASSESSMENTS, values, KEY_ID + " = ?", new String[] {String.valueOf(a.getId())});
        }
        return rowsUpdated > 0;
    }

    public long addAssessment(Assessment a) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ASSESSMENT_TITLE, a.getTitle());
        values.put(KEY_ASSESSMENT_START_DATE, DATE_FORMAT.format(a.getStartDate()));
        values.put(KEY_ASSESSMENT_END_DATE, DATE_FORMAT.format(a.getEndDate()));
        switch (a.getType()) {
            case PERFORMANCE:
                values.put(KEY_ASSESSMENT_TYPE, "PERFORMANCE");
                break;
            case OBJECTIVE:
                values.put(KEY_ASSESSMENT_TYPE, "OBJECTIVE");
        }
        values.put(KEY_ASSIGNED_COURSE_ID, -1);
        return db.insert(TABLE_ASSESSMENTS, null, values);
    }

    public boolean deleteAssessment(Assessment a) {
        SQLiteDatabase db = this.getWritableDatabase();
        int rowsDeleted = db.delete(TABLE_ASSESSMENTS, KEY_ID + " = ?", new String[]{String.valueOf(a.getId())});
        return rowsDeleted > 0;
    }

    public ArrayList<Instructor> getSavedInstructors() {
        ArrayList<Instructor> instructors = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_INSTRUCTORS;
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                Instructor i = new Instructor();
                i.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                i.setName(c.getString(c.getColumnIndex(KEY_INSTRUCTOR_NAME)));
                i.setPhoneNumber(c.getString(c.getColumnIndex(KEY_INSTRUCTOR_PHONE)));
                i.setEmail(c.getString(c.getColumnIndex(KEY_INSTRUCTOR_EMAIL)));
                i.setAssignedCourseId(c.getInt(c.getColumnIndex(KEY_ASSIGNED_COURSE_ID)));
                instructors.add(i);
            } while (c.moveToNext());
        }
        c.close();
        return instructors;
    }

    public ArrayList<Instructor> getAssignedInstructors(int id) {
        ArrayList<Instructor> instructors = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_INSTRUCTORS + " WHERE " + KEY_ASSIGNED_COURSE_ID + " = ?";
        Cursor c = db.rawQuery(selectQuery, new String[] {String.valueOf(id)});
        if (c.moveToFirst()) {
            do {
                Instructor i = new Instructor();
                i.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                i.setName(c.getString(c.getColumnIndex(KEY_INSTRUCTOR_NAME)));
                i.setPhoneNumber(c.getString(c.getColumnIndex(KEY_INSTRUCTOR_PHONE)));
                i.setEmail(c.getString(c.getColumnIndex(KEY_INSTRUCTOR_EMAIL)));
                i.setAssignedCourseId(id);
                instructors.add(i);
            } while (c.moveToNext());
        }
        c.close();
        return instructors;
    }

    public int updateAssignedInstructors(Course course) {
        int rowsUpdated = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        for (Instructor i : getAssignedInstructors(course.getId())) {
            ContentValues values = new ContentValues();
            values.put(KEY_ASSIGNED_COURSE_ID, -1);
            rowsUpdated += db.update(TABLE_INSTRUCTORS, values, KEY_ID + " = ?", new String[] {String.valueOf(i.getId())});
        }

        for (Instructor i : course.getCourseInstructors()) {
                ContentValues values = new ContentValues();
                values.put(KEY_ASSIGNED_COURSE_ID, course.getId());
                rowsUpdated += db.update(TABLE_INSTRUCTORS, values, KEY_ID + " = ?", new String[] {String.valueOf(i.getId())});
        }
        return rowsUpdated;
    }

    public boolean updateInstructor(Instructor i) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_INSTRUCTOR_NAME, i.getName());
        values.put(KEY_INSTRUCTOR_EMAIL, i.getEmail());
        values.put(KEY_INSTRUCTOR_PHONE, i.getPhoneNumber());
        int rowsUpdated = db.update(TABLE_INSTRUCTORS, values, KEY_ID + " = ?", new String[]{String.valueOf(i.getId())});
        return rowsUpdated > 0;
    }

    public long addInstructor(Instructor i) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_INSTRUCTOR_NAME, i.getName());
        values.put(KEY_INSTRUCTOR_EMAIL, i.getEmail());
        values.put(KEY_INSTRUCTOR_PHONE, i.getPhoneNumber());
        values.put(KEY_ASSIGNED_COURSE_ID, -1);
        return db.insert(TABLE_INSTRUCTORS, null, values);
    }

    public boolean deleteInstructor(Instructor i) {
        SQLiteDatabase db = this.getWritableDatabase();
        int rowsDeleted = db.delete(TABLE_INSTRUCTORS, KEY_ID + " = ?", new String[]{String.valueOf(i.getId())});
        return rowsDeleted > 0;
    }
}
