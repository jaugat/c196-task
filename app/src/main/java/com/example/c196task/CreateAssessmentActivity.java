package com.example.c196task;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class CreateAssessmentActivity extends AppCompatActivity {

    private EditText titleField;
    private TextView titleFieldError;
    private Spinner startDateMonthSpinner;
    private Spinner startDateDaySpinner;
    private Spinner startDateYearSpinner;
    private Spinner endDateMonthSpinner;
    private Spinner endDateDaySpinner;
    private Spinner endDateYearSpinner;
    private TextView dateError;
    private Spinner typeSpinner;
    private Button createButton;
    private DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_assessment);
        db = DatabaseHelper.getInstance(this);
        Toolbar toolbar = findViewById(R.id.create_assessment_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        titleField = findViewById(R.id.create_assessment_name_field);
        titleFieldError = findViewById(R.id.create_assessment_name_error);
        startDateMonthSpinner = findViewById(R.id.create_assessment_start_date_month_spinner);
        ArrayAdapter<CharSequence> startDateMonthAdapter = ArrayAdapter.createFromResource(this,
                R.array.date_month_list, android.R.layout.simple_spinner_item);
        startDateMonthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        startDateMonthSpinner.setAdapter(startDateMonthAdapter);
        startDateDaySpinner = findViewById(R.id.create_assessment_start_date_day_spinner);
        ArrayAdapter<CharSequence> startDateDayAdapter = ArrayAdapter.createFromResource(this,
                R.array.date_day_list, android.R.layout.simple_spinner_item);
        startDateDayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        startDateDaySpinner.setAdapter(startDateDayAdapter);
        startDateYearSpinner = findViewById(R.id.create_assessment_start_date_year_spinner);
        ArrayAdapter<CharSequence> startDateYearAdapter = ArrayAdapter.createFromResource(this,
                R.array.date_year_list, android.R.layout.simple_spinner_item);
        startDateYearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        startDateYearSpinner.setAdapter(startDateYearAdapter);

        endDateMonthSpinner = findViewById(R.id.create_assessment_end_date_month_spinner);
        ArrayAdapter<CharSequence> endDateMonthAdapter = ArrayAdapter.createFromResource(this,
                R.array.date_month_list, android.R.layout.simple_spinner_item);
        endDateMonthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        endDateMonthSpinner.setAdapter(endDateMonthAdapter);
        endDateDaySpinner = findViewById(R.id.create_assessment_end_date_day_spinner);
        ArrayAdapter<CharSequence> endDateDayAdapter = ArrayAdapter.createFromResource(this,
                R.array.date_day_list, android.R.layout.simple_spinner_item);
        endDateDayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        endDateDaySpinner.setAdapter(endDateDayAdapter);
        endDateYearSpinner = findViewById(R.id.create_assessment_end_date_year_spinner);
        ArrayAdapter<CharSequence> endDateYearAdapter = ArrayAdapter.createFromResource(this,
                R.array.date_year_list, android.R.layout.simple_spinner_item);
        endDateYearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        endDateYearSpinner.setAdapter(endDateYearAdapter);
        dateError = findViewById(R.id.create_assessment_date_error);
        typeSpinner = findViewById(R.id.create_assessment_type_spinner);
        ArrayAdapter<CharSequence> typeAdapter = ArrayAdapter.createFromResource(this,
                R.array.assessment_type_list, android.R.layout.simple_spinner_item);
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeSpinner.setAdapter(typeAdapter);
        createButton = findViewById(R.id.create_assessment_save_button);
        createButton.setOnClickListener(v -> {
            Assessment assessment = new Assessment();
            dateError.setText("");
            titleFieldError.setText("");
            switch (typeSpinner.getSelectedItem().toString()) {
                case "Performance":
                    assessment.setType(Assessment.Type.PERFORMANCE);
                    break;
                case "Objective":
                    assessment.setType(Assessment.Type.OBJECTIVE);
                    break;
            }
            if (titleField.getText().toString().equals("")) {
                titleFieldError.setText("Name field can't be empty");
            } else {
                assessment.setTitle(titleField.getText().toString());
                try {
                    LocalDate startLd = LocalDate.of(Integer.parseInt(startDateYearSpinner.getSelectedItem().toString()), startDateMonthSpinner.getSelectedItemPosition() + 1, Integer.parseInt(startDateDaySpinner.getSelectedItem().toString()));
                    Date startDate = Date.from(startLd.atStartOfDay(ZoneId.systemDefault()).toInstant());
                    LocalDate endLd = LocalDate.of(Integer.parseInt(endDateYearSpinner.getSelectedItem().toString()), endDateMonthSpinner.getSelectedItemPosition() + 1, Integer.parseInt(endDateDaySpinner.getSelectedItem().toString()));
                    Date endDate = Date.from(endLd.atStartOfDay(ZoneId.systemDefault()).toInstant());
                    assessment.setStartDate(startDate);
                    assessment.setEndDate(endDate);
                    db.addAssessment(assessment);
                    Toast.makeText(this, "Assessment successfully created", Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(this, MainActivity.class);
                    intent.putExtra("FRAGMENT_LAYOUT_ID", R.layout.fragment_assessments);
                    startActivity(intent);

                } catch (DateTimeException e) {
                    dateError.setText("Invalid date(s) entered");
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}