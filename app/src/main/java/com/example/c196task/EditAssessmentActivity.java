package com.example.c196task;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public class EditAssessmentActivity extends AppCompatActivity {

    private Assessment assessment;
    private EditText titleField;
    private TextView titleFieldError;
    private Spinner endDateMonthSpinner;
    private Spinner endDateDaySpinner;
    private Spinner endDateYearSpinner;
    private TextView dateError;
    private Spinner typeSpinner;
    private Button saveButton;
    private DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_assessment);
        db = DatabaseHelper.getInstance(this);
        assessment = db.getAssessment(getIntent().getIntExtra("ASSESSMENT_ID", -1));
        Toolbar toolbar = findViewById(R.id.edit_assessment_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        titleField = findViewById(R.id.edit_assessment_name_field);
        titleField.setText(assessment.getTitle());
        titleFieldError = findViewById(R.id.edit_assessment_name_error);

        Calendar oldEndDate = Calendar.getInstance();
        oldEndDate.setTime(assessment.getEndDate());

        endDateMonthSpinner = findViewById(R.id.edit_assessment_end_date_month_spinner);
        ArrayAdapter<CharSequence> endDateMonthAdapter = ArrayAdapter.createFromResource(this,
                R.array.date_month_list, android.R.layout.simple_spinner_item);
        endDateMonthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        endDateMonthSpinner.setAdapter(endDateMonthAdapter);
        endDateMonthSpinner.setSelection(oldEndDate.get(Calendar.MONTH));

        endDateDaySpinner = findViewById(R.id.edit_assessment_end_date_day_spinner);
        ArrayAdapter<CharSequence> endDateDayAdapter = ArrayAdapter.createFromResource(this,
                R.array.date_day_list, android.R.layout.simple_spinner_item);
        endDateDayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        endDateDaySpinner.setAdapter(endDateDayAdapter);
        endDateDaySpinner.setSelection(oldEndDate.get(Calendar.DAY_OF_MONTH) - 1);

        endDateYearSpinner = findViewById(R.id.edit_assessment_end_date_year_spinner);
        ArrayAdapter<CharSequence> endDateYearAdapter = ArrayAdapter.createFromResource(this,
                R.array.date_year_list, android.R.layout.simple_spinner_item);
        endDateYearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        endDateYearSpinner.setAdapter(endDateYearAdapter);
        endDateYearSpinner.setSelection(oldEndDate.get(Calendar.YEAR) - 2021);

        dateError = findViewById(R.id.edit_assessment_date_error);

        typeSpinner = findViewById(R.id.edit_assessment_type_spinner);
        ArrayAdapter<CharSequence> typeAdapter = ArrayAdapter.createFromResource(this,
                R.array.assessment_type_list, android.R.layout.simple_spinner_item);
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeSpinner.setAdapter(typeAdapter);
        switch (assessment.getType()) {
            case PERFORMANCE:
                typeSpinner.setSelection(0);
                break;
            case OBJECTIVE:
                typeSpinner.setSelection(1);
                break;
        }

        saveButton = findViewById(R.id.edit_assessment_save_button);
        saveButton.setOnClickListener(v -> {
            dateError.setText("");
            titleFieldError.setText("");
            switch (typeSpinner.getSelectedItem().toString()) {
                case "Performance":
                    assessment.setType(Assessment.Type.PERFORMANCE);
                    break;
                case "Objective":
                    assessment.setType(Assessment.Type.OBJECTIVE);
                    break;
            }
            if (titleField.getText().toString().equals("")) {
                titleFieldError.setText("Name field can't be empty");
            } else {
                assessment.setTitle(titleField.getText().toString());
                try {
                    LocalDate endLd = LocalDate.of(Integer.parseInt(endDateYearSpinner.getSelectedItem().toString()), endDateMonthSpinner.getSelectedItemPosition() + 1, Integer.parseInt(endDateDaySpinner.getSelectedItem().toString()));
                    Date endDate = Date.from(endLd.atStartOfDay(ZoneId.systemDefault()).toInstant());
                    assessment.setEndDate(endDate);
                    db.updateAssessment(assessment);
                    Toast.makeText(this, "Assessment successfully updated", Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(this, MainActivity.class);
                    intent.putExtra("FRAGMENT_LAYOUT_ID", R.layout.fragment_assessments);
                    startActivity(intent);

                } catch (DateTimeException e) {
                    dateError.setText("Invalid date(s) entered");
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}