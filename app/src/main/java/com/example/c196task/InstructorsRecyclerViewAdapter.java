package com.example.c196task;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class InstructorsRecyclerViewAdapter extends RecyclerView.Adapter {

    private ArrayList<Instructor> instructors;
    private Context context;
    private DatabaseHelper db;

    public InstructorsRecyclerViewAdapter(ArrayList<Instructor> instructors, Context context) {
        this.instructors = instructors;
        this.context = context;
        db = DatabaseHelper.getInstance(context);
    }

    @NonNull
    @NotNull
    @Override
    public InstructorsRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_instructor, parent, false);
        InstructorsRecyclerViewAdapter.ViewHolder holder = new InstructorsRecyclerViewAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        InstructorsRecyclerViewAdapter.ViewHolder viewHolder = (InstructorsRecyclerViewAdapter.ViewHolder) holder;
        viewHolder.recyclerInstructorName.setText(instructors.get(position).getName());
        viewHolder.recyclerInstructorPhone.setText(instructors.get(position).getPhoneNumber());
        viewHolder.recyclerInstructorEmail.setText(instructors.get(position).getEmail());
        viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Instructor " + instructors.get(position).getName() + " selected", Toast.LENGTH_LONG);
            }
        });
    }

    @Override
    public int getItemCount() {
        return instructors.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView recyclerInstructorName;
        TextView recyclerInstructorPhone;
        TextView recyclerInstructorEmail;
        RelativeLayout parentLayout;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            recyclerInstructorName = itemView.findViewById(R.id.instructor_recycler_entry_name);
            recyclerInstructorPhone = itemView.findViewById(R.id.instructor_recycler_phone);
            recyclerInstructorEmail = itemView.findViewById(R.id.instructor_recycler_email);
            parentLayout = itemView.findViewById(R.id.instructor_list_item_layout);
        }
    }
}
