package com.example.c196task;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class EditNotesActivity extends AppCompatActivity {

    private Course course;
    private EditText note1, note2, note3;
    private Button saveButton;
    private DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_notes);
        db = DatabaseHelper.getInstance(this);
        course = db.getCourse(getIntent().getIntExtra("COURSE_ID", -1));
        Toolbar toolbar = findViewById(R.id.edit_notes_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        note1 = findViewById(R.id.edit_notes_text_area_1);
        note1.setText(course.getNote(0));
        note2 = findViewById(R.id.edit_notes_text_area_2);
        note2.setText(course.getNote(1));
        note3 = findViewById(R.id.edit_notes_text_area_3);
        note3.setText(course.getNote(2));
        saveButton = findViewById(R.id.edit_notes_save_button);
        saveButton.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Are you sure you wish to save changes?")
                    .setPositiveButton("Yes", (dialog, which) -> {
                        ArrayList<String> newNotes = new ArrayList<>();
                        newNotes.add(note1.getText().toString());
                        newNotes.add(note2.getText().toString());
                        newNotes.add(note3.getText().toString());
                        course.setNotes(newNotes);
                        db.updateCourse(course);
                        Toast.makeText(getApplicationContext(), "Changes saved successfully", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), ViewCourseNotesActivity.class);
                        intent.putExtra("COURSE_ID", course.getId());
                        startActivity(intent);
                    }).setNegativeButton("No", null);
            AlertDialog alert = builder.create();
            alert.show();
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}