package com.example.c196task;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class AddInstructorRecyclerAdapter extends RecyclerView.Adapter {

    public static final String TAG = "AddInstructorRecyclerAdapter";

    private Context context;
    private DatabaseHelper db;
    private ArrayList<Instructor> instructors;
    private ArrayList<Instructor> checkedInstructors;

    public AddInstructorRecyclerAdapter(ArrayList<Instructor> instructors, Context context) {
        this.instructors = instructors;
        this.context = context;
        db = DatabaseHelper.getInstance(this.context);
        checkedInstructors = new ArrayList<>();
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_add_instructor, parent, false);
        AddInstructorRecyclerAdapter.ViewHolder holder = new AddInstructorRecyclerAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        AddInstructorRecyclerAdapter.ViewHolder viewHolder = (AddInstructorRecyclerAdapter.ViewHolder) holder;
        viewHolder.recyclerInstructorTitle.setText(instructors.get(position).getName());
        viewHolder.recyclerCheckBox.setText("Add");
        viewHolder.recyclerCheckBox.setChecked(false);
        viewHolder.parentLayout.setOnClickListener(v -> {
            viewHolder.recyclerCheckBox.toggle();
            if (viewHolder.recyclerCheckBox.isChecked()) {
                checkedInstructors.add(instructors.get(position));
                for (Instructor instructor : checkedInstructors) {
                    Log.d(TAG, "Instructor name " + instructor.getName() + " is checked");
                }
            } else if (!viewHolder.recyclerCheckBox.isChecked()) {
                checkedInstructors.remove(instructors.get(position));
                for (Instructor instructor : checkedInstructors) {
                    Log.d(TAG, "Instructor name " + instructor.getName() + " is checked");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return instructors.size();
    }

    public ArrayList<Instructor> getCheckedInstructors() {
        return checkedInstructors;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView recyclerInstructorTitle;
        CheckBox recyclerCheckBox;
        RelativeLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            recyclerInstructorTitle = itemView.findViewById(R.id.add_instructor_recycler_entry_name);
            recyclerCheckBox = itemView.findViewById(R.id.add_instructor_check_box);
            parentLayout = itemView.findViewById(R.id.add_instructor_list_item_layout);
        }
    }
}
