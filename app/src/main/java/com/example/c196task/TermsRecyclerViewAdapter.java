package com.example.c196task;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.util.ArrayList;

public class TermsRecyclerViewAdapter extends RecyclerView.Adapter {

    private static final String TAG = "TermsRecyclerViewAdapter";

    private ArrayList<Term> terms;
    private Context context;

    public TermsRecyclerViewAdapter(ArrayList<Term> termNames, Context context) {
        this.terms = termNames;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_term, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull @org.jetbrains.annotations.NotNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.recyclerTermName.setText(terms.get(position).getName());
        viewHolder.parentLayout.setOnClickListener(v -> {
            Intent intent = new Intent(context, TermDetailedView.class);
            intent.putExtra("TERM_ID", terms.get(position).getId());
            context.startActivity(intent);

        });
    }

    @Override
    public int getItemCount() {
        return terms.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView recyclerTermName;
        RelativeLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            recyclerTermName = itemView.findViewById(R.id.term_recycler_entry_name);
            parentLayout = itemView.findViewById(R.id.term_list_item_layout);
        }
    }
}
