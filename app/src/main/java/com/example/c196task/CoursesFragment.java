package com.example.c196task;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CoursesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CoursesFragment extends Fragment {

    private ArrayList<Course> courses = new ArrayList<>();
    private DatabaseHelper db;
    private Button createButton;


    public CoursesFragment() {
        // Required empty public constructor
    }


    public static CoursesFragment newInstance() {
        CoursesFragment fragment = new CoursesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        db = DatabaseHelper.getInstance(getContext());
        View view = inflater.inflate(R.layout.fragment_courses, container, false);
        courses.addAll(db.getSavedCourses());
        RecyclerView recyclerView = view.findViewById(R.id.courses_recycler_view);
        CoursesRecyclerViewAdapter adapter = new CoursesRecyclerViewAdapter(db.getSavedCourses(), getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        createButton = view.findViewById(R.id.create_course_button);
        createButton.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), CreateCourseActivity.class);
            startActivity(intent);
        });
        return view;
    }
}