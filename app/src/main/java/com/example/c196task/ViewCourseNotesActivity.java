package com.example.c196task;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Button;

import java.util.ArrayList;

public class ViewCourseNotesActivity extends AppCompatActivity {

    public static final String TAG = "ViewCourseNotesActivity";

    private Course course;
    private RecyclerView notesRecycler;
    private Button editButton;
    private Button shareButton;
    private DatabaseHelper db;
    private ArrayList<String> notes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_course_notes);
        Toolbar toolbar = findViewById(R.id.course_notes_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        db = DatabaseHelper.getInstance(this);
        course = db.getCourse(getIntent().getIntExtra("COURSE_ID", -1));
        notesRecycler = findViewById(R.id.notes_recycler);
        NotesRecyclerViewAdapter adapter = new NotesRecyclerViewAdapter(course.getAllNotes(), this);
        notesRecycler.setAdapter(adapter);
        notesRecycler.setLayoutManager(new LinearLayoutManager(this));

        editButton = findViewById(R.id.edit_notes_button);
        editButton.setOnClickListener(v -> {
            Intent intent = new Intent(this, EditNotesActivity.class);
            intent.putExtra("COURSE_ID", course.getId());
            startActivity(intent);
        });
        shareButton = findViewById(R.id.share_notes_button);
        shareButton.setOnClickListener(v -> {
            Log.d(TAG, "Share button clicked.");
            String notesString = "Note 1: '" + course.getNote(0) + "'\n" +
                    "Note 2: '" + course.getNote(1) + "'\n" +
                    "Note 3: '" + course.getNote(2) + "'";
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, notesString);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(Intent.createChooser(intent, "Share Notes"));
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}