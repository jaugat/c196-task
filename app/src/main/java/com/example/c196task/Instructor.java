package com.example.c196task;

public class Instructor {

    private int id;
    private String name;
    private String phoneNumber;
    private String email;
    private int assignedCourseId;

    public Instructor() {
        this.id = 0;
        this.name = "";
        this.phoneNumber = "";
        this.email = "";
        this.assignedCourseId = -1;
    }

    public Instructor(String name, String phoneNumber, String email) {
        this.id = 0;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.assignedCourseId = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAssignedCourseId() {
        return assignedCourseId;
    }

    public void setAssignedCourseId(int assignedCourseId) {
        this.assignedCourseId = assignedCourseId;
    }
}
