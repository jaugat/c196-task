package com.example.c196task;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class AddCourseRecyclerAdapter extends RecyclerView.Adapter {

    public static final String TAG = "AddCourseRecyclerAdapter";

    private Context context;
    private DatabaseHelper db;
    private ArrayList<Course> courses;
    private ArrayList<Course> checkedCourses;

    public AddCourseRecyclerAdapter(ArrayList<Course> courses, Context context) {
        this.courses = courses;
        this.context = context;
        db = DatabaseHelper.getInstance(this.context);
        checkedCourses = new ArrayList<>();
    }

    @NonNull
    @NotNull
    @Override
    public AddCourseRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_add_course, parent, false);
        AddCourseRecyclerAdapter.ViewHolder holder = new AddCourseRecyclerAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        AddCourseRecyclerAdapter.ViewHolder viewHolder = (AddCourseRecyclerAdapter.ViewHolder) holder;
        viewHolder.recyclerCourseTitle.setText(courses.get(position).getTitle());
        viewHolder.recyclerCheckBox.setText("Add");
        viewHolder.recyclerCheckBox.setChecked(false);
        viewHolder.parentLayout.setOnClickListener(v -> {
            viewHolder.recyclerCheckBox.toggle();
            if (viewHolder.recyclerCheckBox.isChecked()) {
                checkedCourses.add(courses.get(position));
                for (Course course : checkedCourses) {
                    Log.d(TAG, "Course name " + course.getTitle() + " is checked");
                }
            } else if (!viewHolder.recyclerCheckBox.isChecked()) {
                checkedCourses.remove(courses.get(position));
                for (Course course : checkedCourses) {
                    Log.d(TAG, "Course name " + course.getTitle() + " is checked");
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return courses.size();
    }

    public ArrayList<Course> getCheckedCourses() {
        return checkedCourses;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView recyclerCourseTitle;
        CheckBox recyclerCheckBox;
        RelativeLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            recyclerCourseTitle = itemView.findViewById(R.id.add_course_recycler_entry_name);
            recyclerCheckBox = itemView.findViewById(R.id.add_course_check_box);
            parentLayout = itemView.findViewById(R.id.add_course_list_item_layout);
        }
    }
}
