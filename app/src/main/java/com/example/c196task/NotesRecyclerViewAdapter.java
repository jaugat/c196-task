package com.example.c196task;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class NotesRecyclerViewAdapter extends RecyclerView.Adapter {

    private ArrayList<String> notes;
    private Context context;

    public NotesRecyclerViewAdapter(ArrayList<String> notes, Context context) {
        this.notes = notes;
        this.context = context;
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_note, parent, false);
        NotesRecyclerViewAdapter.ViewHolder holder = new NotesRecyclerViewAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        NotesRecyclerViewAdapter.ViewHolder viewHolder = (NotesRecyclerViewAdapter.ViewHolder) holder;
        viewHolder.recyclerNote.setText(notes.get(position));
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView recyclerNote;
        RelativeLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            recyclerNote = itemView.findViewById(R.id.note_recycler_text);
            parentLayout = itemView.findViewById(R.id.note_list_item_layout);
        }
    }
}
