package com.example.c196task;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class Course {

    private static final String TAG = "CourseClass";
    private int id;
    private String title;
    private Date startDate;
    private Date endDate;
    private Status status;
    private ArrayList<String> notes;
    private ArrayList<Assessment> assessments;
    private ArrayList<Instructor> courseInstructors;
    private int assignedTermId;

    public enum Status {IN_PROGRESS, COMPLETED, DROPPED, PLAN_TO_TAKE, UNKNOWN}

    public Course(String title, Date startDate, Date endDate, Status status, ArrayList<Assessment> assessments, ArrayList<Instructor> courseInstructors) {
        this.id = 0;
        this.title = title;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
        this.notes = new ArrayList<>();
        this.assessments = assessments;
        this.courseInstructors = courseInstructors;
        this.assignedTermId = -1;
    }

    public Course(String title, Date startDate, Date endDate, Status status) {
        this.id = 0;
        this.title = title;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
        this.notes = new ArrayList<>();
        this.assessments = new ArrayList<>();
        this.courseInstructors = new ArrayList<>();
        this.assignedTermId = -1;

    }

    public Course() {
        this.id = 0;
        this.title = "";
        this.startDate = null;
        this.endDate = null;
        this.status = Status.UNKNOWN;
        this.notes = new ArrayList<>();
        this.assessments = new ArrayList<>();
        this.courseInstructors = new ArrayList<>();
        this.assignedTermId = -1;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(String status) {
        switch (status) {
            case "IN_PROGRESS":
                this.status = Status.IN_PROGRESS;
                break;
            case "COMPLETED":
                this.status = Status.COMPLETED;
                break;
            case "DROPPED":
                this.status = Status.DROPPED;
                break;
            case "PLAN_TO_TAKE":
                this.status = Status.PLAN_TO_TAKE;
                break;
            case "UNKNOWN":
                this.status = Status.UNKNOWN;
                break;
        }
    }

    public boolean addNote(String note) {
        if (notes.size() <= 3) {
            notes.add(note);
            return true;
        } else {
            return false;
        }
    }

    public String getNote(int index) {
        if (!notes.isEmpty()) {
            return notes.get(index);
        } else {
            return "";
        }
    }

    public ArrayList<String> getAllNotes() {
        ArrayList<String> strings = new ArrayList<>();
        for (String note : notes) {
            strings.add(note);
        }
        return strings;
    }

    public void setNotes (ArrayList<String> notes) {
        this.notes = notes;
    }

    public void removeNote(int index) {
        notes.remove(index);
    }

    public ArrayList<Instructor> getCourseInstructors() {
        return courseInstructors;
    }

    public Instructor getPrimaryInstructor() {
        if (courseInstructors.size() >= 1) {
            return courseInstructors.get(0);
        } else {
            return null;
        }
    }

    public void setCourseInstructors(ArrayList<Instructor> courseInstructors) {
        this.courseInstructors = courseInstructors;
    }

    public void addCourseInstructor(Instructor instructor) {
        instructor.setAssignedCourseId(id);
        courseInstructors.add(instructor);
    }

    public void removeCourseInstructor(Instructor instructor) {
        if (courseInstructors.contains(instructor)) {
            courseInstructors.remove(instructor);
        } else {
            Log.d(TAG, "removeCourseInstructor: instructor not found");
        }
    }

    public ArrayList<Assessment> getAssessments() {
        return assessments;
    }

    public void setAssessments(ArrayList<Assessment> assessments) {
        this.assessments = assessments;
    }

    public void addAssessment(Assessment assessment) {
        assessment.setAssignedCourseId(id);
        assessments.add(assessment);
    }

    public void removeAssessment(Assessment assessment) {
        if(assessments.contains(assessment)) {
            assessment.setAssignedCourseId(-1);
            assessments.remove(assessment);
        }else {
            Log.d(TAG, "removeAssessment: assessment not found");
        }
    }

    public int getAssignedTermId() {
        return assignedTermId;
    }

    public void setAssignedTermId(int assignedTermId) {
        this.assignedTermId = assignedTermId;
    }
}
