package com.example.c196task;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static final String TAG = "MainActivity";

    private DrawerLayout drawer;
    private DatabaseHelper db;
    NavigationView navigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = DatabaseHelper.getInstance(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        if (savedInstanceState == null) {
            switch (getIntent().getIntExtra("FRAGMENT_LAYOUT_ID", 0)) {
                case 0:
                case R.layout.fragment_home:
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).addToBackStack(null).commit();
                    navigationView.setCheckedItem(R.id.nav_home);
                    break;
                case R.layout.fragment_terms:
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new TermsFragment()).addToBackStack(null).commit();
                    navigationView.setCheckedItem(R.id.nav_terms);
                    break;
                case R.layout.fragment_courses:
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new CoursesFragment()).addToBackStack(null).commit();
                    navigationView.setCheckedItem(R.id.nav_courses);
                    break;
                case R.layout.fragment_assessments:
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AssessmentsFragment()).addToBackStack(null).commit();
                    navigationView.setCheckedItem(R.id.nav_assessments);
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).addToBackStack(null).commit();
                navigationView.setCheckedItem(R.id.nav_home);
                break;
            case R.id.nav_terms:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new TermsFragment()).addToBackStack(null).commit();
                navigationView.setCheckedItem(R.id.nav_terms);
                break;
            case R.id.nav_courses:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new CoursesFragment()).addToBackStack(null).commit();
                navigationView.setCheckedItem(R.id.nav_courses);
                break;
            case R.id.nav_assessments:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AssessmentsFragment()).addToBackStack(null).commit();
                navigationView.setCheckedItem(R.id.nav_assessments);
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}