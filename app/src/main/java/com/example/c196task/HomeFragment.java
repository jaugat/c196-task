package com.example.c196task;

import android.content.res.Configuration;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.LayoutInflater;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {

    private Button termsButton;
    private Button coursesButton;
    private Button assessmentsButton;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        NavigationView navigationView = getActivity().findViewById(R.id.nav_view);
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        termsButton = view.findViewById(R.id.terms_button);
        termsButton.setOnClickListener(v -> {
            try {
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, new TermsFragment()).addToBackStack(null).commit();
                navigationView.setCheckedItem(R.id.nav_terms);
            } catch (NullPointerException e) {
                System.out.println("Fragment manager is null");
            }
        });
        coursesButton = view.findViewById(R.id.courses_button);
        coursesButton.setOnClickListener(v -> {
            try {
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, new CoursesFragment()).addToBackStack(null).commit();
                navigationView.setCheckedItem(R.id.nav_courses);
            } catch (NullPointerException e) {
                System.out.println("Fragment manager is null");
            }
        });
        assessmentsButton = view.findViewById(R.id.assessments_button);
        assessmentsButton.setOnClickListener(v -> {
            try {
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, new AssessmentsFragment()).addToBackStack(null).commit();
                navigationView.setCheckedItem(R.id.nav_assessments);
            } catch (NullPointerException e) {
                System.out.println("Fragment manager is null");
            }
        });
        return view;
    }
}