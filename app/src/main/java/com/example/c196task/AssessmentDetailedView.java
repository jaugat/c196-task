package com.example.c196task;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Calendar;

public class AssessmentDetailedView extends AppCompatActivity {

    private Assessment assessment;
    private TextView assessmentTitle;
    private TextView assessmentStartDate;
    private Button startDateAlarmButton;
    private TextView assessmentEndDate;
    private Button endDateAlarmButton;
    private TextView assessmentType;
    private DatabaseHelper db;
    private DateFormat df = DateFormat.getDateInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assessment_detailed_view);
        Toolbar toolbar = findViewById(R.id.detailed_assessment_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        db = DatabaseHelper.getInstance(this);
        assessment = db.getAssessment(getIntent().getIntExtra("ASSESSMENT_ID", -1));
        assessmentTitle = findViewById(R.id.detailed_assessment_title);
        assessmentTitle.setText(assessment.getTitle());
        assessmentStartDate = findViewById(R.id.detailed_assessment_start_date);
        assessmentStartDate.setText(df.format(assessment.getStartDate()));
        startDateAlarmButton = findViewById(R.id.detailed_assessment_start_date_alarm_button);
        startDateAlarmButton.setOnClickListener(v -> {
            final EditText input = new EditText(this);
            input.setInputType(InputType.TYPE_CLASS_NUMBER);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Days before start date:")
                    .setView(input)
                    .setPositiveButton("Ok", (dialog, which) -> {
                        int days = Integer.parseInt(input.getText().toString());
                        Calendar c = Calendar.getInstance();
                        c.setTime(assessment.getStartDate());
                        c.set(Calendar.HOUR_OF_DAY, 12);
                        c.set(Calendar.MINUTE, 0);
                        c.set(Calendar.SECOND, 0);
                        c.add(Calendar.DAY_OF_YEAR, -(days));
                        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                        Intent intent = new Intent(this, AlertReceiver.class);
                        intent.putExtra("OBJECT_TYPE", "ASSESSMENT");
                        intent.putExtra("COURSE_ID", assessment.getId());
                        intent.putExtra("DATE_TYPE", "Start date");
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent, 0);
                        alarmManager.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pendingIntent);
                        Toast.makeText(getApplicationContext(), "Alarm set for " + days + " days before start date.", Toast.LENGTH_LONG).show();
                    }).setNegativeButton("Cancel", null);
            AlertDialog dialog = builder.create();
            dialog.show();
        });
        assessmentEndDate = findViewById(R.id.detailed_assessment_end_date);
        assessmentEndDate.setText(df.format(assessment.getEndDate()));
        endDateAlarmButton = findViewById(R.id.detailed_assessment_end_date_alarm_button);
        endDateAlarmButton.setOnClickListener(v -> {
            final EditText input = new EditText(this);
            input.setInputType(InputType.TYPE_CLASS_NUMBER);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Days before end date:")
                    .setView(input)
                    .setPositiveButton("Ok", (dialog, which) -> {
                        int days = Integer.parseInt(input.getText().toString());
                        Calendar c = Calendar.getInstance();
                        c.setTime(assessment.getStartDate());
                        c.set(Calendar.HOUR_OF_DAY, 12);
                        c.set(Calendar.MINUTE, 0);
                        c.set(Calendar.SECOND, 0);
                        c.add(Calendar.DAY_OF_YEAR, -(days));
                        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                        Intent intent = new Intent(this, AlertReceiver.class);
                        intent.putExtra("OBJECT_TYPE", "ASSESSMENT");
                        intent.putExtra("COURSE_ID", assessment.getId());
                        intent.putExtra("DATE_TYPE", "End date");
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent, 0);
                        alarmManager.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pendingIntent);
                        Toast.makeText(getApplicationContext(), "Alarm set for " + days + " days before End date.", Toast.LENGTH_LONG).show();
                    }).setNegativeButton("Cancel", null);
            AlertDialog dialog = builder.create();
            dialog.show();
        });
        assessmentType = findViewById(R.id.detailed_assessment_type);
        switch (assessment.getType()) {
            case OBJECTIVE:
                assessmentType.setText("OBJECTIVE ASSESSMENT");
                break;
            case PERFORMANCE:
                assessmentType.setText("PERFORMANCE ASSESSMENT");
                break;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detailed_view_toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.edit_item_button:
                Intent intent = new Intent(this, EditAssessmentActivity.class);
                intent.putExtra("ASSESSMENT_ID", assessment.getId());
                startActivity(intent);
                break;
            case R.id.delete_item_button:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Are you sure you want to delete " + assessment.getTitle() + "?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                db.deleteAssessment(assessment);
                                Toast.makeText(getApplicationContext(), "Assessment deleted", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.putExtra("FRAGMENT_LAYOUT_ID", R.layout.fragment_assessments);
                                startActivity(intent);
                            }
                        }).setNegativeButton("No", null);
                AlertDialog alert = builder.create();
                alert.show();
        }

        return super.onOptionsItemSelected(item);
    }
}