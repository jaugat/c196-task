package com.example.c196task;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class Assessment {

    public static final String TAG = "AssessmentClass";
    private int id;
    private String title;
    private Date startDate;
    private Date endDate;
    private Type type;
    public enum Type {PERFORMANCE, OBJECTIVE}
    private int assignedCourseId;

    public Assessment() {
        this.id = 0;
        this.title = "";
        this.startDate = null;
        this.endDate = null;
        this.type = null;
        this.assignedCourseId = -1;
    }

    public Assessment(String title, Date startDate, Date endDate, Type type) {
        this.id = 0;
        this.title = title;
        this.startDate = startDate;
        this.endDate = endDate;
        this.type = type;
        this.assignedCourseId = -1;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Type getType() {
        return type;
    }

    public void setType(String type) {
        switch (type) {
            case "PERFORMANCE":
                this.type = Type.PERFORMANCE;
                break;
            case "OBJECTIVE":
                this.type = Type.OBJECTIVE;
                break;
        }
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getAssignedCourseId() {
        return assignedCourseId;
    }

    public void setAssignedCourseId(int assignedCourseId) {
        this.assignedCourseId = assignedCourseId;
    }
}
