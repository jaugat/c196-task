package com.example.c196task;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;

public class TermsFragment extends Fragment {

    public static final String TAG = "TermsFragment";
    private ArrayList<Term> terms = new ArrayList<>();
    private DatabaseHelper db;
    private Button createButton;
    RecyclerView recyclerView;
    TermsRecyclerViewAdapter adapter;


    public TermsFragment() {
        // Required empty public constructor
    }

    public static TermsFragment newInstance() {
        TermsFragment fragment = new TermsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        db = DatabaseHelper.getInstance(getContext());
        View view = inflater.inflate(R.layout.fragment_terms, container, false);
        terms.addAll(db.getSavedTerms());
        recyclerView = view.findViewById(R.id.terms_recycler_view);
        adapter = new TermsRecyclerViewAdapter(terms, getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        createButton = view.findViewById(R.id.terms_create_button);
        createButton.setOnClickListener(v -> {
            Intent intent = new Intent (getContext(), CreateTermActivity.class);
            startActivity(intent);
        });
        return view;
    }
}