package com.example.c196task;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.util.ArrayList;

public class AssessmentsRecyclerViewAdapter extends RecyclerView.Adapter {

    private static final String TAG = "AssessmentsRecyclerViewAdapter";

    private ArrayList<Assessment> assessments;
    private Context context;

    public AssessmentsRecyclerViewAdapter(ArrayList<Assessment> assessments, Context context) {
        this.assessments = assessments;
        this.context = context;
    }

    @NonNull
    @Override
    public AssessmentsRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_assessment, parent, false);
        AssessmentsRecyclerViewAdapter.ViewHolder holder = new AssessmentsRecyclerViewAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull @org.jetbrains.annotations.NotNull RecyclerView.ViewHolder holder, int position) {
        AssessmentsRecyclerViewAdapter.ViewHolder viewHolder = (AssessmentsRecyclerViewAdapter.ViewHolder) holder;
        viewHolder.recyclerAssessmentTitle.setText(assessments.get(position).getTitle());
        if (assessments.get(position).getEndDate() == null) {
            viewHolder.recyclerAssessmentDate.setText("No end date entered");
        } else {
            DateFormat df = DateFormat.getDateInstance();
            viewHolder.recyclerAssessmentDate.setText(df.format(assessments.get(position).getEndDate()));
        }
        Assessment.Type type = assessments.get(position).getType();
        switch (type) {
            case OBJECTIVE:
                viewHolder.recyclerAssessmentType.setText("Objective Assessment");
                break;
            case PERFORMANCE:
                viewHolder.recyclerAssessmentType.setText("Performance Assessment");
                break;
        }
        viewHolder.parentLayout.setOnClickListener(v -> {
            Intent intent = new Intent(context, AssessmentDetailedView.class);
            intent.putExtra("ASSESSMENT_ID", assessments.get(position).getId());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return assessments.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView recyclerAssessmentTitle;
        TextView recyclerAssessmentDate;
        TextView recyclerAssessmentType;
        RelativeLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            recyclerAssessmentTitle = itemView.findViewById(R.id.assessment_recycler_entry_name);
            recyclerAssessmentDate = itemView.findViewById(R.id.assessment_recycler_end_date);
            recyclerAssessmentType = itemView.findViewById(R.id.assessment_recycler_type);
            parentLayout = itemView.findViewById(R.id.assessment_list_item_layout);
        }
    }
}