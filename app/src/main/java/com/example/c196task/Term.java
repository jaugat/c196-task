package com.example.c196task;

import android.util.Log;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class Term {

    public static final String TAG = "TermClass";
    private int id;
    private String name;
    private Date startDate;
    private Date endDate;
    private ArrayList<Course> assignedCourses;

    public Term() {
        this.id = 0;
        this.name = "";
        this.startDate = null;
        this.endDate = null;
        this.assignedCourses = new ArrayList<>();
    }

    public Term(String name, Date startDate, Date endDate, ArrayList<Course> assignedCourses) {
        this.id = 0;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.assignedCourses = new ArrayList<>(assignedCourses);
    }

    public Term(String name, Date startDate, Date endDate) {
        this.id = 0;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.assignedCourses = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public ArrayList<Course> getAssignedCourses() {
        return assignedCourses;
    }

    public void setAssignedCourses(ArrayList<Course> assignedCourses) {
        this.assignedCourses = assignedCourses;
    }

    public void addCourse(Course course) {
        course.setAssignedTermId(id);
        assignedCourses.add(course);
    }

    public void removeCourse(Course course) {
        if (assignedCourses.contains(course)) {
            course.setAssignedTermId(-1);
            assignedCourses.remove(course);
        } else {
            System.out.println(course.getTitle() + " not assigned to " + name);
        }
    }
}
