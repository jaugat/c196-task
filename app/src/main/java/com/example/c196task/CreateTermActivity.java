package com.example.c196task;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

public class CreateTermActivity extends AppCompatActivity {

    public static final String TAG = "CreateTermActivity";

    private EditText nameField;
    private TextView nameFieldError;
    private Spinner startDateMonthSpinner;
    private Spinner startDateDaySpinner;
    private Spinner startDateYearSpinner;
    private TextView dateError;
    private Spinner endDateMonthSpinner;
    private Spinner endDateDaySpinner;
    private Spinner endDateYearSpinner;
    private RecyclerView addCourseRecyclerView;
    private Button createButton;
    private DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_term);
        db = DatabaseHelper.getInstance(this);
        Toolbar toolbar = findViewById(R.id.create_term_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        nameField = findViewById(R.id.create_term_name_field);
        nameFieldError = findViewById(R.id.create_term_name_error);
        startDateMonthSpinner = findViewById(R.id.create_term_start_date_month_spinner);
        ArrayAdapter<CharSequence> startDateMonthAdapter = ArrayAdapter.createFromResource(this,
                R.array.date_month_list, android.R.layout.simple_spinner_item);
        startDateMonthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        startDateMonthSpinner.setAdapter(startDateMonthAdapter);
        startDateDaySpinner = findViewById(R.id.create_term_start_date_day_spinner);
        ArrayAdapter<CharSequence> startDateDayAdapter = ArrayAdapter.createFromResource(this,
                R.array.date_day_list, android.R.layout.simple_spinner_item);
        startDateDayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        startDateDaySpinner.setAdapter(startDateDayAdapter);

        startDateYearSpinner = findViewById(R.id.create_term_start_date_year_spinner);
        ArrayAdapter<CharSequence> startDateYearAdapter = ArrayAdapter.createFromResource(this,
                R.array.date_year_list, android.R.layout.simple_spinner_item);
        startDateYearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        startDateYearSpinner.setAdapter(startDateYearAdapter);

        dateError = findViewById(R.id.create_term_date_error);
        endDateMonthSpinner = findViewById(R.id.create_term_end_date_month_spinner);
        ArrayAdapter<CharSequence> endDateMonthAdapter = ArrayAdapter.createFromResource(this,
                R.array.date_month_list, android.R.layout.simple_spinner_item);
        endDateMonthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        endDateMonthSpinner.setAdapter(endDateMonthAdapter);

        endDateDaySpinner = findViewById(R.id.create_term_end_date_day_spinner);
        ArrayAdapter<CharSequence> endDateDayAdapter = ArrayAdapter.createFromResource(this,
                R.array.date_day_list, android.R.layout.simple_spinner_item);
        endDateDayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        endDateDaySpinner.setAdapter(endDateDayAdapter);
        endDateYearSpinner = findViewById(R.id.create_term_end_date_year_spinner);
        ArrayAdapter<CharSequence> endDateYearAdapter = ArrayAdapter.createFromResource(this,
                R.array.date_year_list, android.R.layout.simple_spinner_item);
        endDateYearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        endDateYearSpinner.setAdapter(endDateYearAdapter);
        addCourseRecyclerView = findViewById(R.id.create_term_add_courses_recycler);
        AddCourseRecyclerAdapter addCoursesAdapter = new AddCourseRecyclerAdapter(db.getSavedCourses(), this);

        addCourseRecyclerView.setAdapter(addCoursesAdapter);
        addCourseRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        createButton = findViewById(R.id.create_term_button);
        createButton.setOnClickListener(v -> {
            Term term = new Term();
            dateError.setText("");
            nameFieldError.setText("");
            if (nameField.getText().toString().equals("")) {
                nameFieldError.setText("Name field can't be empty");
            } else {
                term.setName(nameField.getText().toString());
                try {
                    LocalDate startLd = LocalDate.of(Integer.parseInt(startDateYearSpinner.getSelectedItem().toString()), startDateMonthSpinner.getSelectedItemPosition() + 1, Integer.parseInt(startDateDaySpinner.getSelectedItem().toString()));
                    Date startDate = Date.from(startLd.atStartOfDay(ZoneId.systemDefault()).toInstant());
                    LocalDate endLd = LocalDate.of(Integer.parseInt(endDateYearSpinner.getSelectedItem().toString()), endDateMonthSpinner.getSelectedItemPosition() + 1, Integer.parseInt(endDateDaySpinner.getSelectedItem().toString()));
                    Date endDate = Date.from(endLd.atStartOfDay(ZoneId.systemDefault()).toInstant());
                    if (startDate.after(endDate)) {
                        dateError.setText("Start date must be before end date");
                    } else {
                        term.setStartDate(startDate);
                        term.setEndDate(endDate);
                        term.setAssignedCourses(addCoursesAdapter.getCheckedCourses());
                        term.setId((int) db.addTerm(term));
                        db.updateAssignedCourses(term);
                        Toast.makeText(this, "Term successfully created", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(this, MainActivity.class);
                        intent.putExtra("FRAGMENT_LAYOUT_ID", R.layout.fragment_terms);
                        startActivity(intent);
                    }
                } catch (DateTimeException e) {
                    dateError.setText("Invalid date(s) entered");
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}