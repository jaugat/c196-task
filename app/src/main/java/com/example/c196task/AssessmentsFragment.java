package com.example.c196task;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AssessmentsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AssessmentsFragment extends Fragment {

    private ArrayList<Assessment> assessments = new ArrayList<>();
    private DatabaseHelper db;
    private Button createButton;

    public AssessmentsFragment() {
        // Required empty public constructor
    }


    public static AssessmentsFragment newInstance() {
        AssessmentsFragment fragment = new AssessmentsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        db = DatabaseHelper.getInstance(getContext());
        View view = inflater.inflate(R.layout.fragment_assessments, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.assessments_recycler_view);
        AssessmentsRecyclerViewAdapter adapter = new AssessmentsRecyclerViewAdapter(db.getSavedAssessments(), getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        createButton = view.findViewById(R.id.create_assessment_button);
        createButton.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), CreateAssessmentActivity.class);
            startActivity(intent);
        });
        return view;
    }
}