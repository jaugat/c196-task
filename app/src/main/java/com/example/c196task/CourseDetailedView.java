package com.example.c196task;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Calendar;

public class CourseDetailedView extends AppCompatActivity {

    public static final String TAG = "CourseDetailedViewClass";
    private Course course;
    private TextView courseTitle;
    private TextView startDate;
    private Button startDateAlarmButton;
    private TextView endDate;
    private Button endDateAlarmButton;
    private TextView status;
    private RecyclerView assessmentsList;
    private RecyclerView instructorsList;
    private Button viewNotesButton;
    private DatabaseHelper db;
    private DateFormat df = DateFormat.getDateInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_detailed_view);
        db = DatabaseHelper.getInstance(this);
        course = db.getCourse(getIntent().getIntExtra("COURSE_ID", -1));
        Toolbar toolbar = findViewById(R.id.detailed_course_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        courseTitle = findViewById(R.id.detailed_course_title);
        courseTitle.setText(course.getTitle());
        startDate = findViewById(R.id.detailed_course_start_date);
        startDate.setText(df.format(course.getStartDate()));
        startDateAlarmButton = findViewById(R.id.detailed_course_start_date_alarm_button);
        startDateAlarmButton.setOnClickListener(v -> {
            final EditText input = new EditText(this);
            input.setInputType(InputType.TYPE_CLASS_NUMBER);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Days before start date:")
                    .setView(input)
                    .setPositiveButton("Ok", (dialog, which) -> {
                        int days = Integer.parseInt(input.getText().toString());
                        Calendar c = Calendar.getInstance();
                        c.setTime(course.getStartDate());
                        c.set(Calendar.HOUR_OF_DAY, 12);
                        c.set(Calendar.MINUTE, 0);
                        c.set(Calendar.SECOND, 0);
                        c.add(Calendar.DAY_OF_YEAR, -(days));
                        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                        Intent intent = new Intent(this, AlertReceiver.class);
                        intent.putExtra("OBJECT_TYPE", "COURSE");
                        intent.putExtra("COURSE_ID", course.getId());
                        intent.putExtra("DATE_TYPE", "Start date");
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent, 0);
                        alarmManager.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pendingIntent);
                        Toast.makeText(getApplicationContext(), "Alarm set for " + days + " days before start date.", Toast.LENGTH_LONG).show();
                    }).setNegativeButton("Cancel", null);
            AlertDialog dialog = builder.create();
            dialog.show();

        });
        endDate = findViewById(R.id.detailed_course_end_date);
        endDate.setText(df.format(course.getEndDate()));
        endDateAlarmButton = findViewById(R.id.detailed_course_end_date_alarm_button);
        endDateAlarmButton.setOnClickListener(v -> {
            final EditText input = new EditText(this);
            input.setInputType(InputType.TYPE_CLASS_NUMBER);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Days before end date:")
                    .setView(input)
                    .setPositiveButton("Ok", (dialog, which) -> {
                        int days = Integer.parseInt(input.getText().toString());
                        Calendar c = Calendar.getInstance();
                        c.setTime(course.getStartDate());
                        c.set(Calendar.HOUR_OF_DAY, 12);
                        c.set(Calendar.MINUTE, 0);
                        c.set(Calendar.SECOND, 0);
                        c.add(Calendar.DAY_OF_YEAR, -(days));
                        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                        Intent intent = new Intent(this, AlertReceiver.class);
                        intent.putExtra("COURSE_ID", course.getId());
                        intent.putExtra("OBJECT_TYPE", "COURSE");
                        intent.putExtra("DATE_TYPE", "End date");
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent, 0);
                        alarmManager.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pendingIntent);
                        Toast.makeText(getApplicationContext(), "Alarm set for " + days + " days before end date.", Toast.LENGTH_LONG).show();
                    }).setNegativeButton("Cancel", null);
            AlertDialog dialog = builder.create();
            dialog.show();
        });
        status = findViewById(R.id.detailed_course_status);
        switch (course.getStatus()) {
            case DROPPED:
                status.setText("DROPPED");
                break;
            case UNKNOWN:
                status.setText("UNKNOWN STATUS");
                break;
            case COMPLETED:
                status.setText("COMPLETED");
                break;
            case IN_PROGRESS:
                status.setText("IN PROGRESS");
                break;
            case PLAN_TO_TAKE:
                status.setText("PLAN TO TAKE");
                break;
        }
        assessmentsList = findViewById(R.id.detailed_course_assessment_list);
        AssessmentsRecyclerViewAdapter assessmentsAdapter = new AssessmentsRecyclerViewAdapter(db.getAssignedAssessments(course.getId()), this);
        assessmentsList.setAdapter(assessmentsAdapter);
        assessmentsList.setLayoutManager(new LinearLayoutManager(this));
        instructorsList = findViewById(R.id.detailed_course_instructor_list);
        InstructorsRecyclerViewAdapter instructorsAdapter = new InstructorsRecyclerViewAdapter(db.getAssignedInstructors(course.getId()), this);
        instructorsList.setAdapter(instructorsAdapter);
        instructorsList.setLayoutManager(new LinearLayoutManager(this));
        viewNotesButton = findViewById(R.id.detailed_course_view_notes_button);
        viewNotesButton.setOnClickListener(v -> {
            Intent intent = new Intent(this, ViewCourseNotesActivity.class);
            intent.putExtra("COURSE_ID", course.getId());
            startActivity(intent);
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detailed_view_toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.edit_item_button:
                Intent intent = new Intent(this, EditCourseActivity.class);
                intent.putExtra("COURSE_ID", course.getId());
                startActivity(intent);
                break;
            case R.id.delete_item_button:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Are you sure you want to delete " + course.getTitle() + "?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                db.deleteCourse(course);
                                Toast.makeText(getApplicationContext(), "Course deleted", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.putExtra("FRAGMENT_LAYOUT_ID", R.layout.fragment_courses);
                                startActivity(intent);
                            }
                        }).setNegativeButton("No", null);
                AlertDialog alert = builder.create();
                alert.show();
        }

        return super.onOptionsItemSelected(item);
    }
}