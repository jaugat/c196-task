package com.example.c196task;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class TermDetailedView extends AppCompatActivity {

    public static final String TAG = "TermDetailedView";

    private Term term;
    private TextView termName;
    private TextView termStartDate;
    private TextView termEndDate;
    private RecyclerView courseList;
    private DatabaseHelper db;
    private DateFormat df = DateFormat.getDateInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_detailed_view);
        db = DatabaseHelper.getInstance(this);
        term = db.getTerm(getIntent().getIntExtra("TERM_ID", -1));
        Toolbar toolbar = findViewById(R.id.detailed_term_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        termName = findViewById(R.id.detailed_term_name);
        termName.setText(term.getName());
        termStartDate = findViewById(R.id.detailed_term_start_date);
        termStartDate.setText(df.format(term.getStartDate()));
        termEndDate = findViewById(R.id.detailed_term_end_date);
        termEndDate.setText(df.format(term.getEndDate()));
        courseList = findViewById(R.id.detailed_term_course_list);
        CoursesRecyclerViewAdapter adapter = new CoursesRecyclerViewAdapter(db.getAssignedCourses(term.getId()), this);
        courseList.setAdapter(adapter);
        courseList.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detailed_view_toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.edit_item_button:
                Intent intent = new Intent(this, EditTermActivity.class);
                intent.putExtra("TERM_ID", term.getId());
                startActivity(intent);
                break;
            case R.id.delete_item_button:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Are you sure you want to delete " + term.getName() + "?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (!term.getAssignedCourses().isEmpty()) {
                                    Toast.makeText(getApplicationContext(), "Term has assigned courses! Delete failed!", Toast.LENGTH_LONG).show();
                                } else {
                                    db.deleteTerm(term);
                                    Toast.makeText(getApplicationContext(), "Term deleted", Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    intent.putExtra("FRAGMENT_LAYOUT_ID", R.layout.fragment_terms);
                                    startActivity(intent);
                                }

                            }
                        }).setNegativeButton("No", null);
                AlertDialog alert = builder.create();
                alert.show();
        }

        return super.onOptionsItemSelected(item);
    }
}