package com.example.c196task;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class AddAssessmentRecyclerAdapter extends RecyclerView.Adapter {

    public static final String TAG = "AddAssessmentRecyclerAdapter";

    private Context context;
    private DatabaseHelper db;
    private ArrayList<Assessment> assessments;
    private ArrayList<Assessment> checkedAssessments;

    public AddAssessmentRecyclerAdapter(ArrayList<Assessment> assessments, Context context) {
        this.assessments = assessments;
        this.context = context;
        db = DatabaseHelper.getInstance(this.context);
        checkedAssessments = new ArrayList<>();
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_add_assessment, parent, false);
        AddAssessmentRecyclerAdapter.ViewHolder holder = new AddAssessmentRecyclerAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        AddAssessmentRecyclerAdapter.ViewHolder viewHolder = (AddAssessmentRecyclerAdapter.ViewHolder) holder;
        viewHolder.recyclerAssessmentTitle.setText(assessments.get(position).getTitle());
        viewHolder.recyclerCheckBox.setText("Add");
        viewHolder.recyclerCheckBox.setChecked(false);
        viewHolder.parentLayout.setOnClickListener(v -> {
            viewHolder.recyclerCheckBox.toggle();
            if (viewHolder.recyclerCheckBox.isChecked()) {
                checkedAssessments.add(assessments.get(position));
                for (Assessment assessment : checkedAssessments) {
                    Log.d(TAG, "Assessment name " + assessment.getTitle() + " is checked");
                }
            } else if (!viewHolder.recyclerCheckBox.isChecked()) {
                checkedAssessments.remove(assessments.get(position));
                for (Assessment assessment : checkedAssessments) {
                    Log.d(TAG, "Assessment name " + assessment.getTitle() + " is checked");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return assessments.size();
    }

    public ArrayList<Assessment> getCheckedAssessments() {
        return checkedAssessments;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView recyclerAssessmentTitle;
        CheckBox recyclerCheckBox;
        RelativeLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            recyclerAssessmentTitle = itemView.findViewById(R.id.add_assessment_recycler_entry_name);
            recyclerCheckBox = itemView.findViewById(R.id.add_assessment_check_box);
            parentLayout = itemView.findViewById(R.id.add_assessment_list_item_layout);
        }
    }
}
